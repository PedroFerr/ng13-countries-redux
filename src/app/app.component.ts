import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { CountriesActions, CountryActions } from './_store/actions';
import * as fromCountries from './_store/store-pack.selectors';

import { Country, Currency } from './models/country';
import { Region } from './models/regions';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    title = 'countries by region';

    regions$: Observable<Region[]>;
    countries$: Observable<Country[]> | undefined;
    currentCountry$: Observable<Country> | undefined;

    validateData: string = '';

    constructor(private store: Store) {
        this.regions$ = store.select(fromCountries.selectRegions);
    }

    getSelected(option: string) {
        if (option === 'Europe' || option === 'Asia') {
            this.getAllCountries(option);
        } else {
            this.getCountry(option);
        }

        // Should validate (OUT of 'Europe' and 'Asia') "dataValid" on <app-detail />:
        this.validateData = option;
    };


    getAllCountries(region: string) {
        this.store.dispatch(CountriesActions.getCountries({ region }));
        this.countries$ = this.store.select(fromCountries.selectCountries);
    };

    getCountry(name: string) {
        this.store.dispatch(CountryActions.getCountry({ name }));
        this.currentCountry$ = this.store.select(fromCountries.selectSelectedCountry);
    }
}
