import { createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromCountries from './reducers/countries.reducer';


export const selectCountriesState = createFeatureSelector<fromCountries.State>(
    fromCountries.countriesFeatureKey
);

export const selectCountriesEntitiesState = createSelector(
    selectCountriesState,
    (state) => state
);



export const selectRegions = createSelector(
    selectCountriesState,
    fromCountries.getRegions
);
export const selectCountries = createSelector(
    selectCountriesState,
    fromCountries.getCountries
);
export const selectSelectedCountry = createSelector(
    selectCountriesEntitiesState,
    fromCountries.getCountry
);



// Error:
export const selectCountriesError = createSelector(
    selectCountriesState,
    fromCountries.getError
);
