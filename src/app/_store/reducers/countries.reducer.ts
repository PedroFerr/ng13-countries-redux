import { Action, createReducer, on } from '@ngrx/store';

import { CountriesActions, CountriesApiActions, CountryActions } from '../actions';
import { Region } from '../../models/regions';
import { Country, Currency } from '../../models/country';


export const countriesFeatureKey = 'countries';

export interface State {
    regions: Region[];
    countries: Country[];
    region: string;
    country: any;
    // country: Country;
    error: string;
}

export const initialState: State = {
    regions: [{ 'name': 'Europe' }, { 'name': 'Asia' }],
    countries: [],
    region: '',
    country: {},
    // country: {
    //     name: '',
    //     capital: '',
    //     population: 0,
    //     flag: '',
    // },
    error: ''
};

export const countriesReducer = createReducer(
    initialState,
    on(CountriesActions.getCountries, (state, { region }) => {
        return {
            ...state,
            error: '',
            region
        };
    }),
    on(CountriesApiActions.getCountriesSuccess, (state, { countries }) => {
        return {
            ...state,
            error: '',
            countries,
            region: state.region,
        };
    }),
    on(CountriesApiActions.getCountriesFailure, (state, { errorMsg }) => {
        return {
            ...state,
            error: errorMsg
        };
    }),

    on(CountryActions.getCountry, (state, { name }) => {
        return {
          ...state,
          error: '',
          country: state.countries.find((country) => country.name === name)
        };
    })
);

export const getRegions = (state: State) => state.regions;

export const getCountries = (state: State) => state.countries;

export const getCountry = (state: State) => state.country;

export const getError = (state: State) => state.error;

