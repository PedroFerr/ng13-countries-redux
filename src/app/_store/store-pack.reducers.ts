import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromCountries from './reducers/countries.reducer';


export interface CountriesState {
    [fromCountries.countriesFeatureKey]: fromCountries.State;
}

export const reducers: ActionReducerMap<CountriesState> = {
    [fromCountries.countriesFeatureKey]: fromCountries.countriesReducer
};


// console.log of the several states
export function logger(reducer: ActionReducer<CountriesState>): ActionReducer<CountriesState> {
    return (state, action) => {
        const result = reducer(state, action);
        console.groupCollapsed(action.type);
        console.log('prev state', state);
        console.log('action', action);
        console.log('next state', result);
        console.groupEnd();
        return result;
    };
}
export const metaReducers: MetaReducer<CountriesState>[] = !environment.production ? [logger] : [];

