import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

import { Country } from '../models/country';

@Injectable({
    providedIn: 'root'
})
export class CountriesService {

    apiUrl = `${environment.endPoint}`;

    constructor(private http: HttpClient) { }

    getCountries(region: string): Observable<Country[]> {
        return this.http.get<Country[]>(`${this.apiUrl}${region}`);
    };    

}
