import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { reducers, metaReducers } from './_store/store-pack.reducers';
import { CountriesEffects } from './_store/effects/countries.effects';

import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectComponent } from './components/select/select.component';
import { DetailsComponent } from './components/details/details.component';
import { AngularMaterialModule } from './app-material.module';

@NgModule({
    declarations: [
        AppComponent,
        SelectComponent,
        DetailsComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AngularMaterialModule,

        HttpClientModule,
        FormsModule, ReactiveFormsModule,
        
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                // strictStateImmutability and strictActionImmutability are enabled by default
                strictStateSerializability: true,
                strictActionSerializability: true,
                strictActionWithinNgZone: true,
                strictActionTypeUniqueness: true,
            },
        }),
        !environment.production ? StoreDevtoolsModule.instrument({
            name: 'Countries by Region',
            maxAge: 25, logOnly: environment.production
        }) : [],
        EffectsModule.forRoot([CountriesEffects])

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
