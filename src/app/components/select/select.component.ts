import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
    
    dataControl = new FormControl('')

    @Input() data: any | undefined;
    @Input() choosenData: string | undefined;
    @Output() selectedItem = new EventEmitter<any>();

    constructor() { }

    ngOnInit() {
    }

}
