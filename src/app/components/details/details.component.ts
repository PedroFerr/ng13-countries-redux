import { Component, Input, OnInit } from '@angular/core';
import { Country, Currency } from 'src/app/models/country';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

    imageLoader = true;
    
    @Input() details: any   // Country;
    @Input() dataValid: string = '';

    constructor() { }

    ngOnInit() {
    }
}
